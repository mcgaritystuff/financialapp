FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y nodejs npm postgresql-client
RUN mkdir /financeapp
WORKDIR /financeapp
COPY Gemfile /financeapp/Gemfile
COPY Gemfile.lock /financeapp/Gemfile.lock
RUN gem install bundler -v 2.0.2
RUN bundle update --bundler
RUN bundle install
COPY . /financeapp
RUN npm install -g yarn

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE @@PORT_NUMBER@@

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
