# How to setup

Of course, pull down the project. And from there, you'll need to configure a few little things. First, you need to generate some users. This application assumes you will have 1 - 3 users and no more (the UI was only designed for 2 users since this is for personal usage). To start, just run the install script with:

```bash
./installer.sh
```

After the installer finishes, you should be able to start the software with `docker-compose up -d` and then reach it in the browser using the port number you specified in the installer (i.e. `http://localhost:30021`).