class AccountsController < ApplicationController
  def new
    @account = Account.new
    @user = User.find_by_id(params[:user_id])
  end

  def edit
    @account = Account.find_by_id(params[:id])
    @user = @account.user
  end

  def create
    cc_acct = params[:account][:account_type] == 'credit_card'
    loan_acct = params[:account][:account_type] == 'loan'
    @user = User.find_by_id(params[:user_id])

    @account = Account.new(
      name: params[:account][:name], 
      credit_card: cc_acct, 
      loan: loan_acct,
      user: @user
    )
    
    respond_to do |format|
      if @account.save
        @user_accounts = @user.accounts
        format.js
      else
        format.js { render :new }
      end
    end
  end

  def update
    @account = Account.find_by_id(params[:id])
    @account.name = params[:account][:name]

    respond_to do |format|
      if @account.save
        @user = @account.user
        @user_accounts = @user.accounts
        format.js
      else
        format.js { render :edit }
      end
    end
  end

  def destroy
    @account = Account.find_by_id(params[:id])
    @user = @account.user
    
    respond_to do |format|
      if @account.destroy
        format.js
      end
    end
  end
end
