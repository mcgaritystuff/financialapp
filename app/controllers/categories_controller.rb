class CategoriesController < ApplicationController
  include CategoriesHelper
  def index
    @categories = Category.all
    @disabled_categories = get_disabled_categories
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    @disabled_categories = get_disabled_categories
    
    respond_to do |format|
      if @category.save
        format.html { render partial: 'categories/modal/category_row', :locals => {:category => @category} }
      else
        format.js { render(:status => 500, :json => @category.errors.full_messages)}
      end
    end
  end

  def edit
    @category = Category.find_by_id(params[:id])
  end

  def update
    @category = Category.find_by_id(params[:id])
    @disabled_categories = get_disabled_categories
    other = Category.find_by(name: 'Other')
    other_cannot_update = ["Cannot update the 'Other' category"] if @category.id == other.id
    
    respond_to do |format|
      if not other_cannot_update and @category.update(category_params)
        format.html { render partial: 'categories/modal/category_row', :locals => {:category => @category} }
      else
        format.js { render(:status => 500, :json => other_cannot_update || @category.errors.full_messages)}
      end
    end
  end

  def destroy
    category = Category.find_by_id(params[:id])
    transactions_with_category = Transaction.where(category_id: category.id)
    other = Category.find_by(name: 'Other')
    @disabled_categories = get_disabled_categories

    if category.id != other.id # don't allow to delete 'Other'
      transactions_with_category.each do |transaction|
        transaction.update_attribute(:category_id, other.id)
      end
      
      # Don't forget to remove the cookie
      if @disabled_categories.include?(category.id)
        @disabled_categories.delete(category.id)
        cookies.permanent[:disabled_categories] = @disabled_categories
      end

      category.destroy
    end
    
    @categories = Category.all
  end

  def toggle
    @categories = Category.all
    @disabled_categories = get_disabled_categories
    param_id = params[:id].to_i
    
    case param_id
    when 0 # Check all categories
      @disabled_categories = []
    when -1 # Uncheck all categories
      @disabled_categories = @categories.map(&:id)
    else # Toggling a category
      if @disabled_categories.include?(param_id)
        @disabled_categories.delete(param_id)
      else
        @disabled_categories << param_id
      end
    end

    cookies.permanent[:disabled_categories] = @disabled_categories
  end

  private
    def category_params
      params.permit(:name, :foreground_color, :background_color)
    end
end
