class StaticPagesController < ApplicationController
  def main
    @user = User.find_by_id(params[:id]) || nil
    @current_year = Time.now.year
    @user_accounts = Account.select { |u| u.user_id == @user.id } if @user
  end
end
