class TransactionsController < ApplicationController
  include TransactionsHelper

  def index
    @account = Account.find_by_id(params[:account_id])
    get_transaction_list_variables(params[:year], @account)
  end

  def new
    @transaction = Transaction.new
    @account = Account.find_by_id(params[:account_id])  
    @categories = Category.all
  end

  def create
    @account = Account.find_by_id(params[:account_id])
    @category = Category.find_by_id(params[:category_id])
    recurrence = params[:recurrence].to_i
    transactions = []
    time_now = (params[:transaction][:date] || Time.now).to_date
    end_of_year = time_now.end_of_year.to_date

    while time_now <= end_of_year
      t = Transaction.new(transaction_params)
      t.account = @account
      t.category = @category
      t.date = time_now # Override for recurrences throughout the loop
      transactions << t

      break if not t.valid? or recurrence == 0

      case recurrence
      when 1 # daily
        time_now += 1.day
      when 2 # weekly
        time_now += 1.week
      when 3 # bi-weekly
        time_now += 2.weeks
      when 4 # tri-weekly
        time_now += 3.weeks
      when 5 # monthly
        time_now += 1.month
      when 6 # bi-monthly
        time_now += 2.months
      else # no (or invalid) recurrence. Shouldn't get here in the first place
        break
      end
    end

    @transaction = transactions.first

    respond_to do |format|
      if @transaction.valid? 
        # Assume all transactions are valid and saved since they're duplicates
        transactions.each do |transaction|
          transaction.save
        end
        get_transaction_list_variables(@transaction.date.year, @account)
        @user_accounts = @user.accounts
        format.js { render :partial => 'transactions/apply_change' }
      else
        @categories = Category.all
        format.js { render :new }
      end
    end
  end

  def destroy
    @transaction = Transaction.find_by_id(params[:id])
    @account = @transaction.account
    get_transaction_list_variables(@transaction.date.year, @account)
    @user_accounts = @user.accounts
    respond_to do |format|
      if @transaction.destroy
        format.js { render :partial => 'transactions/apply_change' }
      end
    end
  end

  def edit
    @transaction = Transaction.find_by_id(params[:id])
    @account = @transaction.account
    @categories = Category.all
  end

  def update
    @transaction = Transaction.find_by_id(params[:id])
    @category = Category.find_by_id(params[:category_id])
    @transaction.update(transaction_params)
    @transaction.update_attribute(:category_id, @category.id) if @category
    @account = @transaction.account
    respond_to do |format|
      if @transaction.valid?
        get_transaction_list_variables(@transaction.date.year, @account)
        @user_accounts = @user.accounts
        @skip_jump = true # Don't jump to today's transaction
        format.js { render :partial => 'transactions/apply_change' }
      else
        @categories = Category.all
        format.js { render :edit }
      end
    end
  end

  private
    def transaction_params
      params.require(:transaction).permit(:date, :name, :details, :cost, :reward_amount)
    end
end
