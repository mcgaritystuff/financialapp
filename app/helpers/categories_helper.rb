module CategoriesHelper
  def get_disabled_categories
    disabled_categories = cookies.permanent[:disabled_categories] || []
    disabled_categories = disabled_categories.split('&').map(&:to_i) unless disabled_categories.is_a?(Array)
    disabled_categories
  end
end
