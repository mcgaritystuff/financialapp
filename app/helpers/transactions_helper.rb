module TransactionsHelper
  def get_transaction_list_variables(year, account)
    @user = account.user
    @respective_transactions = account.transactions.where("cast(strftime('%Y', date) as int) = ?", year)
    @starting_balance = account.prev_years_balance(year)
    @starting_rewards_balance = account.prev_years_reward_balance(year)
    @categories = Category.all
  end
end
