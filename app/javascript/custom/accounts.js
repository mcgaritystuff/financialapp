select_account = (account_id) => {
  var prevAcct = $("input.btn-success")[0]
  if (prevAcct && prevAcct.id != `account-${account_id}`) {
    prevAcct.classList.remove('btn-success')
    prevAcct.classList.add('btn-primary')
  }

  var account = $(`input#account-${account_id}`)[0]
  if (account && !account.classList.contains('btn-success')) {
    account.classList.remove('btn-primary')
    account.classList.add('btn-success')
  }
}

delete_account = (id) => {
  var decision = confirm("Are you sure you want to DELETE this account, its transactions, AND balances? This is irreversible!");
  if (decision) {
    $.ajax({
      url: `/accounts/${id}`,
      type: 'DELETE'
    })
  }
}