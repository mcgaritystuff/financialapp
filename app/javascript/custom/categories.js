uncheck_all_categories = () => {
  $(".category-checkbox").attr('checked', false);
}

check_all_categories = () => {
  $(".category-checkbox").attr('checked', true);
}

save_category = (id) => {
  if (id) {
    save_old_category(id);
  } else {
    save_new_category();
  }
}

save_new_category = () => {
  $.ajax({ 
    url: '/categories',
    type: 'POST',
    data: {
      name: $("#edit_category_name")[0].value,
      background_color: $("#edit_category_background_color")[0].value.toUpperCase(),
      foreground_color: $("#edit_category_foreground_color")[0].value.toUpperCase()
    },
    success: function(new_row) {
      finish_category_editing();
      $('#category-table-contents').append(new_row);
    },
    error: function(error) {
      if (JSON.parse(error.responseText)) {
        alert(JSON.parse(error.responseText).join('\n')); // Displays an error from the server
      } else {
        alert('Something went wrong on the server');
      }
    }
  })
}

save_old_category = (id) => {
  $.ajax({ 
    url: `/categories/${id}`,
    type: 'PATCH',
    data: {
      name: $("#edit_category_name")[0].value,
      background_color: $("#edit_category_background_color")[0].value.toUpperCase(),
      foreground_color: $("#edit_category_foreground_color")[0].value.toUpperCase()
    },
    success: function(edited_row) {
      finish_category_editing();
      $(`#category-row-${id}`).show();
      $(`#category-row-${id}`).replaceWith(edited_row);
    },
    error: function(error) {
      if (JSON.parse(error.responseText)) {
        alert(JSON.parse(error.responseText).join('\n')); // Displays an error from the server
      } else {
        alert('Something went wrong on the server');
      }
    }
  });
  update_transactions_background();
}

cancel_creating_category = (id) => {
  finish_category_editing();
  if (id) {
    $(`#category-row-${id}`).show();
  }
}

finish_category_editing = () => {
  $('#edit-category-row').remove();
  $('#example-section').remove();
  $('#new-category-button').prop('disabled', false);
}

change_example_background = () => {
  $("#example-row")[0].bgColor = $("#edit_category_background_color")[0].value
}

change_example_foreground = () => {
  $("#example-row").css('color', $("#edit_category_foreground_color")[0].value);
}

delete_category = (id) => {
  var decision = confirm("Are you sure you want to DELETE this category? Any transactions with this will be set to 'Other'!");
  if (decision) {
    $.ajax({
      url: `/categories/${id}`,
      type: 'DELETE'
    })
  }
}