refresh_current_info = (net_worth) => {
  $("#net-worth")[0].innerText = `Net Worth: $${net_worth}`;
  var selectedYear = $("select#year")[0].value
  $('input[name="year"]').each( function() { 
    $(this).val(selectedYear)
  });
}

/* hacky solution to focusing on the modal's first field due to its animation*/
focus_on = (element_id) => {
  setTimeout(() => {
    var field = $(element_id).get(0);
    field.focus();
    field.selectionStart = field.selectionEnd = field.value.length;
  }, 150);
}