populate_transactions = () => {
  var selectedYear = $("select#year")[0].value
  $('input[name="year"]').each( function() { 
    $(this).val(selectedYear)
  });
  var account = $("input.btn-success")[0];
  if (account) {
    var account_id = account.id.substr(account.id.indexOf('-')).slice(1)
    $.ajax({
      url: `/accounts/${account_id}/transactions?year=${selectedYear}`
    })
  }
}

update_transactions_background = () => {
  var year = $("select#year")[0].value;
  var account = $("input.btn-account.btn-success")[0];
  if (account && year) {
    var account_id = account.id.substr(account.id.indexOf('-')).slice(1)
    $.ajax({
      url: `/accounts/${account_id}/transactions?year=${year}`
    });
  }
}