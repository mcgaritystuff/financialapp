class Account < ApplicationRecord
  include ActionView::Helpers::NumberHelper
  
  belongs_to :user
  has_many :transactions,  dependent: :destroy 
  has_many :balances,  dependent: :destroy 

  validates :name, presence: true

  def prev_years_balance(year = Time.now.year)
    Balance.find_by(year: (year.to_i - 1), account_id: id)&.total_balance || 0 
  end

  def prev_years_reward_balance(year = Time.now.year)
    Balance.find_by(year: (year.to_i - 1), account_id: id)&.total_reward_balance || 0 
  end

  def current_balance(year: Time.now.year, formatted: false)
    last_year = prev_years_balance year
    if last_year
      balance = last_year
    else
      balance = 0
    end
    transactions_until_now = transactions.where("cast(strftime('%Y', date) as int) = ? AND date <= ?", year, Time.now)
    
    if formatted
      balance += transactions_until_now.sum { |t| t.transaction_cost }
      number_with_precision(balance, precision: 2, delimiter: ',')
    else
      balance += transactions_until_now.sum { |t| t.transaction_cost }
    end
  end
end
