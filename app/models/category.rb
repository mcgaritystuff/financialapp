class Category < ApplicationRecord
  default_scope -> { order('lower(name)')}

  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :background_color, length: { is: 7 }, 
                               format: { with: /#[0-9A-F]{6,6}/i }
  validates :foreground_color, length: { is: 7 }, 
                               format: { with: /#[0-9A-F]{6,6}/i }
end
