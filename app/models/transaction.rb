class Transaction < ApplicationRecord
  include ActionView::Helpers::NumberHelper

  belongs_to :account
  belongs_to :category

  default_scope -> { order(date: :asc) }

  before_create :add_balance, :add_rewards_balance, :validate_balance_object
  after_destroy :remove_balance, :remove_rewards_balance
  before_update :temp_old_values
  after_update :update_balance, :update_rewards_balance

  validates :name, presence: true
  validates :date, presence: true

  def transaction_cost(formatted: false)
    if formatted
      number_with_precision(cost, precision: 2, delimiter: ',')
    else
      cost
    end
  end

  private
    def temp_old_values
      old_transaction = Transaction.find_by_id(id)
      @old_transaction_cost = old_transaction.transaction_cost
      @old_reward_aount = old_transaction.reward_amount
    end

    def change_balance(amount)
      # update all balances from this transaction's year and onward (in case we input a real old one)
      respective_balances = account.balances.where("year >= ?", date.year)
      respective_balances.each do |balance|
        balance.update_attribute(:total_balance, balance.total_balance + amount)
      end
    end

    def change_rewards_balance(amount)
      if account.credit_card
        # update all rewards balances from this transaction's year and onward (in case we input a real old one)
        respective_balances = account.balances.where("year >= ?", date.year)
        respective_balances.each do |balance|
          balance.update_attribute(:total_reward_balance, balance.total_reward_balance + amount)
        end
      end
    end

    def add_balance
      change_balance(transaction_cost)
    end

    def remove_balance
      change_balance(-transaction_cost)
    end

    def update_balance
      change_balance(transaction_cost - @old_transaction_cost)
    end

    def add_rewards_balance
      change_rewards_balance(reward_amount)
    end

    def remove_rewards_balance
      change_rewards_balance(-reward_amount)
    end

    def update_rewards_balance
      change_rewards_balance(reward_amount - @old_reward_aount)
    end

    def validate_balance_object
      respective_balance = account.balances.where("year = ?", date.year).first
      if not respective_balance
        Balance.create(year: date.year, 
                       account_id: account.id, 
                       total_balance: account.prev_years_balance(date.year) + transaction_cost,
                       total_reward_balance: account.prev_years_reward_balance(date.year) + reward_amount)
      end
    end
end
