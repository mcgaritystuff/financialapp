class User < ApplicationRecord
  include ActionView::Helpers::NumberHelper
  
  has_many :accounts

  validates :name, presence: true

  def net_worth(formatted = true)
    total = accounts.sum { |a| a.current_balance || 0 }
    if formatted
      number_with_precision(total, precision: 2, delimiter: ',')
    else
      total
    end
  end
end
