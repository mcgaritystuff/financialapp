Rails.application.routes.draw do
  root "static_pages#main"
  get '/users/:id', to: "static_pages#main", as: 'user'
  get '/accounts/new/:user_id', to: "accounts#new", as: 'new_account'
  post '/categories/toggle/:id', to: 'categories#toggle', as: 'category_toggle'
  
  resources :transactions, only: [:edit, :update, :create, :destroy]
  resources :accounts, only: [:edit, :update, :create, :destroy] do
    resources :transactions, only: [:index, :new]
  end
  resources :categories
end
