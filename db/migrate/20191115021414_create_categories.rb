class CreateCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :categories do |t|
      t.string :name, null: false
      t.string :background_color, default: '#FFFFFF'
      t.string :foreground_color, default: '#000000'

      t.timestamps
    end
  end
end
