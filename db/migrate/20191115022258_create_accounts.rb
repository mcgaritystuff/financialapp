class CreateAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :accounts do |t|
      t.string :name, unique: true, null: false
      t.boolean :credit_card, default: false
      t.boolean :loan, default: false
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
