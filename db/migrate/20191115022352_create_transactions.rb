class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.datetime :date, null: false
      t.text :name, null: false
      t.text :details
      t.float :cost, default: 0
      t.integer :reward_amount, default: 0

      t.references :account, null: false, foreign_key: true
      t.references :category, default: 1, foreign_key: true # Default category = 'Other'
      
      t.timestamps
    end
  end
end
