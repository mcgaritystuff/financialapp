class CreateBalances < ActiveRecord::Migration[6.0]
  def change
    create_table :balances do |t|
      t.integer :year, null: false
      t.float :total_balance, default: 0
      t.integer :total_reward_balance, default: 0
      t.references :account, null: false, foreign_key: true
    end

    add_index :balances, [:year, :account_id], unique: true
  end
end
