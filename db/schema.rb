# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_19_234400) do

  create_table "accounts", force: :cascade do |t|
    t.string "name", null: false
    t.boolean "credit_card", default: false
    t.boolean "loan", default: false
    t.integer "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "balances", force: :cascade do |t|
    t.integer "year", null: false
    t.float "total_balance", default: 0.0
    t.integer "total_reward_balance", default: 0
    t.integer "account_id", null: false
    t.index ["account_id"], name: "index_balances_on_account_id"
    t.index ["year", "account_id"], name: "index_balances_on_year_and_account_id", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "name", null: false
    t.string "background_color", default: "#FFFFFF"
    t.string "foreground_color", default: "#000000"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.datetime "date", null: false
    t.text "name", null: false
    t.text "details"
    t.float "cost", default: 0.0
    t.integer "reward_amount", default: 0
    t.integer "account_id", null: false
    t.integer "category_id", default: 1
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_transactions_on_account_id"
    t.index ["category_id"], name: "index_transactions_on_category_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "accounts", "users"
  add_foreign_key "balances", "accounts"
  add_foreign_key "transactions", "accounts"
  add_foreign_key "transactions", "categories"
end
