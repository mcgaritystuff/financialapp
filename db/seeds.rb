# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user1 = User.create({name: 'User1'})
user2 = User.create({name: 'User2'})

cat0 = Category.create(name: 'Other')
cat1 = Category.create(name: 'Deposit', background_color: '#00FF00')
cat2 = Category.create(name: 'Something', background_color: '#FF0000')
cat3 = Category.create(name: 'Rewards Usage', background_color: '#0000FF', foreground_color: '#FFFFFF')

checking1 = Account.create(name: 'Test Checking', user: user1)
savings1 = Account.create(name: 'Test Savings', user: user1)
checking2 = Account.create(name: 'Test Checking 2', user: user2)
cc1 = Account.create(name: 'Test Credit Card', user: user1, credit_card: true)

Transaction.create(date: 1.year.ago, name: 'Deposit', details: 'Mooneyyyy', cost: 10000, account: checking1, category: cat1)
Transaction.create(date: 1.hour.ago, name: 'Valero', details: 'Gas', cost: -30, account: checking1, category: cat2)
Transaction.create(date: 1.minute.ago, name: 'Blah 2', details: 'More Details', cost: -20, account: checking1, category: cat2)

Transaction.create(name: "womp", details: "more womp 2", cost: -20.00, reward_amount: 40, account: cc1, category: cat2, date: 1.year.ago)
Transaction.create(name: "womp", details: "more womp 2", cost: -30.00, reward_amount: 60, account: cc1, category: cat2, date: Time.now)
Transaction.create(name: "Rewards Use", details: "Used Rewards", cost: 0.00, reward_amount: -30, account: cc1, category: cat3, date: Time.now)
Transaction.create(name: "womp", details: "more womp 2", cost: -50.00, reward_amount: 100, account: cc1, category: cat2, date: Time.now)