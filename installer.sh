#!/bin/bash

PORT_REGEX="^[0-9]+"
touch seeds.rb

function set_port() {
  local -n ref=$1
  while [[ ! "$ref" =~ $PORT_REGEX ]]; do
    read -p "What is the unique port number you want? " ref
    if [[ "$(netstat -tuln | grep ":${ref}")" != "" ]]; then
      echo -e "Port in use!"
      ref=0
    fi;
  done;
}

if [[ -d "tmp" ]]; then
  echo "Please remove the tmp directory in this project folder before continuing"
  exit -1
fi;

echo "Warning! If you have a database already setup, this will overwrite it! CTRL+C to cancel NOW!"

# Get the number of users to put in the database
while [[ ! "$USER_COUNT" =~ [1-3] ]]; do
  read -p "How many users will be using this (1 - 3)? " USER_COUNT
done;

# Get the port to install the app on
set_port APP_PORT

# Replace file entries for port number with requested one
echo "Replacing port numbers in files..."
sed -i'' -e "s/@@PORT_NUMBER@@/${APP_PORT}/g" Dockerfile
sed -i'' -e "s/@@PORT_NUMBER@@/${APP_PORT}/g" docker-compose.yml

# Get all the names that the user wants to have
for (( i=1; i<=$USER_COUNT; i++ )); do
  read -p "Name for user ${i}: " USER_NAME
  echo "Adding a seed for ${USER_NAME}..."
  echo "User.create({name: '${USER_NAME}'})" >> seeds.rb
done;

# Run the new seeds file (without deleting the 'test' one)
mv db/seeds.rb db/seeds.rb_bak
mv seeds.rb db/.

# Run the docker stuffs
docker-compose build
docker-compose run --rm web yarn install --check-files
docker-compose run --rm web rake db:create
docker-compose run --rm web rails db:migrate:reset # In case it was made before and needs to be rewritten
docker-compose run --rm web rails db:seed
docker-compose run --rm web yarn install --check-files # Sometimes the docker container has a fluke for some reason

# Restore the seeds file
mv db/seeds.rb_bak db/seeds.rb

echo "Done! Start it up with the following command and then hit the URL with port ${APP_PORT}: docker-compose up -d"