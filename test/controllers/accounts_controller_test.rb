require 'test_helper'

class AccountsControllerTest < ActionDispatch::IntegrationTest
  test "Should show a new account modal" do
    get new_account_path(users(:john)), xhr: true
    assert_template 'accounts/new'
    assert_no_match '<div id=\"error\">', response.body
  end

  test "Should show the new account modal with errors when something went wrong" do
    post accounts_path(Account.new), xhr: true, params: { account: { account_type: 'na' }, user_id: users(:john).id }
    assert_template 'accounts/new'
    assert_match '<div id=\"error\">', response.body
  end

  test "Should add an account when the correct info has been filled" do
    assert_difference 'Account.count', 1 do
      post accounts_path(Account.new), xhr: true, params: { account: { name: 'blah', account_type: 'na' }, user_id: users(:john).id }
    end
  end

  test "Should not add an account when there was an error" do
    assert_no_difference 'Account.count' do
      post accounts_path(Account.new), xhr: true, params: { account: { account_type: 'na' }, user_id: users(:john).id }
    end
  end

  test "Should get the correct transactions for a checking account" do
    checking = accounts(:john_checking)
    year = 2019
    running_balance = checking.prev_years_balance

    get account_transactions_path(checking), xhr: true, params: { year: year }
    assert_template 'transactions/index'
    checking.transactions.where("cast(strftime('%Y', date) as int) = ?", year).each do |transaction|
      running_balance += transaction.transaction_cost
      assert_match "<td>#{transaction.date.to_date}</td>", response.body
      assert_match "<td title=\"#{transaction.name}\">#{transaction.name}</td>", response.body
      assert_match "<td title=\"#{transaction.details}\">#{transaction.details}</td>", response.body
      assert_match "<td>$#{transaction.transaction_cost(formatted: true)}</td>", response.body
      assert_match "<td>$#{number_with_precision(running_balance, precision: 2, delimiter: ',')}</td>", response.body
      assert_match "<td>#{transaction.category.name}</td>", response.body
    end
  end

  test "Should show the correct columns for credit card accounts" do
    credit_card = accounts(:john_credit_card)
    year = 2019
    running_rewards_balance = credit_card.prev_years_reward_balance

    get account_transactions_path(credit_card), xhr: true, params: { year: year }
    assert_template 'transactions/index'
    credit_card.transactions.where("cast(strftime('%Y', date) as int) = ?", year).each do |transaction|
      running_rewards_balance += transaction.reward_amount
      assert_match "<td>#{transaction.reward_amount}</td>", response.body
      assert_match "<td>#{running_rewards_balance}</td>", response.body
    end
  end
  
  test "Should delete an account" do
    checking = accounts(:john_checking)
    assert_difference 'Account.count', -1 do
      delete account_path(checking), xhr: true
    end
  end
  
  test "Should delete account transactions when the account is deleted" do
    checking = accounts(:john_checking)
  
    assert_difference 'Transaction.count', -checking.transactions.count do
      delete account_path(checking), xhr: true
    end
  end

  test "Should delete account balances when the account is deleted" do
    checking = accounts(:john_checking)
  
    assert_difference 'Balance.count', -checking.balances.count do
      delete account_path(checking), xhr: true
    end
  end

  test "Should properly update an account when edited" do
    checking = accounts(:john_checking)
    old_name = checking.name
    new_name = "#{old_name}_2"

    patch account_path(checking), xhr: true, params: { account: { name: new_name } }
    assert_equal new_name, Account.find_by_id(checking.id).name
  end
end
