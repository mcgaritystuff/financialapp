require 'test_helper'

class CategoriesControllerTest < ActionDispatch::IntegrationTest
  def setup
    def regex(category)
      /enable_#{category.name.parameterize(separator: "_")}[^\n]*checked/
    end
    def checked_element(response, category)
      response.body.split('\n').select{|line| line.match?(/checkbox.*#{category.name.parameterize(separator: "_")}/)}.first
    end
  end

  test "Should render all check boxes as checked when opening the modal" do
    get categories_path, xhr: true
    assert_template 'categories/index'

    Category.all.each do |category|
      assert_match regex(category), checked_element(response, category)
    end
  end

  test "Should render check boxes as checked only when not stored as a cookie" do
    category_id = categories(:paycheck).id

    post category_toggle_path(category_id), xhr: true
    get categories_path, xhr: true
    assert_template 'categories/index'

    Category.all.each do |category|
      if category.id != category_id
        assert_match regex(category), checked_element(response, category)
      else
        assert_no_match regex(category), checked_element(response, category)
      end
    end
  end

  test "Should disable a checkbox and store it in the cookie" do
    category_id = categories(:paycheck).id
    category = Category.find_by_id(category_id)

    post category_toggle_path(category_id), xhr: true
    get categories_path, xhr: true
    assert_template 'categories/index'
    assert_no_match regex(category), checked_element(response, category)

    post category_toggle_path(category_id), xhr: true
    get categories_path, xhr: true
    assert_match regex(category), checked_element(response, category)
  end

  test "Should uncheck all checkboxes" do
    post category_toggle_path(-1), xhr: true
    get categories_path, xhr: true
    assert_template 'categories/index'
    
    Category.all.each do |category|
      assert_no_match regex(category), checked_element(response, category)
    end
  end
  
  test "Should check all boxes" do
    post category_toggle_path(0), xhr: true
    get categories_path, xhr: true
    assert_template 'categories/index'
    
    Category.all.each do |category|
      assert_match regex(category), checked_element(response, category)
    end
  end

  test "Should delete a category and set the default 'Other' category for respective transactions" do
    paycheck = categories(:paycheck)
    other = categories(:other)
    work_paycheck = transactions(:work_paycheck)

    assert_difference 'Category.count', -1 do
      delete category_path(paycheck.id), xhr: true
    end

    get categories_path, xhr: true
    assert_no_match paycheck.name, response.body
    assert_equal other.name, work_paycheck.reload.category.name
  end

  test "Should edit and save category colors and name" do
    paycheck = categories(:paycheck)
    new_name = "#{paycheck.name}_2"

    patch category_path(paycheck.id), xhr: true, params: { 
                                                  name: new_name, 
                                                  foreground_color: paycheck.foreground_color, 
                                                  background_color: paycheck.background_color
                                                }
    get categories_path, xhr: true
    assert_match new_name, response.body
  end

  test "Should create a new category" do
    cat_name = 'cats'
    cat_fore = '#0F0F0F'
    cat_back = '#F00000'
    assert_difference 'Category.count', 1 do
      post categories_path, xhr: true, params: { 
                                        name: cat_name, 
                                        foreground_color: cat_fore, 
                                        background_color: cat_back 
                                      } 
    end
    get categories_path, xhr: true
    category = Category.find_by(name: cat_name, foreground_color: cat_fore, background_color: cat_back)

    assert category
    assert_match regex(category), response.body
  end

  test "Should not be able to delete the Other category" do
    other = categories(:other)

    assert_no_difference 'Category.count' do
      delete category_path(other.id), xhr: true
    end

    get categories_path, xhr: true
    assert_match other.name, response.body
  end

  test "Should not be able to edit the Other category" do
    other = categories(:other)
    new_name = "#{other.name}_2"

    patch category_path(other.id), xhr: true, params: { 
                                                name: new_name, 
                                                foreground_color: other.foreground_color, 
                                                background_color: other.background_color
                                              }
    get categories_path, xhr: true
    assert_no_match new_name, response.body
  end
end
