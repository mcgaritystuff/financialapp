require 'test_helper'

class TransactionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @account = accounts(:john_checking)
  end

  test "Should create a transaction successfully" do
    category = categories(:bill)
    assert_difference 'Transaction.count', 1 do
      post transactions_path, xhr: true, params: { 
                                            account_id: @account.id,
                                            category_id: category.id,
                                            transaction: { 
                                              date: Time.now,
                                              name: "Blahdeblah",
                                              details: "Informations",
                                              cost: -1.00
                                            }
                                          }
    end
  end

  test "Should fail at creating a transaction" do
    assert_no_difference 'Transaction.count' do
      post transactions_path, xhr: true, params: { 
                                            account_id: @account.id, 
                                            transaction: {
                                              name: "",
                                              details: "",
                                            }
                                          }
    end
  end

  test "Should delete a transaction" do
    transaction = transactions(:gas)
    assert_difference 'Transaction.count', -1 do
      delete transaction_path(transaction), xhr: true
    end
  end

  test "Should update a transaction" do
    transaction = transactions(:gas)
    new_name = "#{transaction.name}_2"
    patch transaction_path(transaction), xhr: true, params: { transaction: { name: new_name } }
    assert_equal new_name, transaction.reload.name
  end

  test "Should create recurring transactions when set at 1/day" do
    travel_to Time.local(2019, 1, 1, 0, 0, 1)
    category = categories(:bill)
    assert_difference 'Transaction.count', 365 do
      post transactions_path, xhr: true, params: { 
                                            account_id: @account.id, 
                                            category_id: category.id,
                                            transaction: { 
                                              date: Time.now,
                                              name: "Blahdeblah",
                                              details: "Informations",
                                              cost: -1.00
                                            },
                                            recurrence: '1'
                                          }
    end
  end

  test "Should create recurring transactions when set at 1/week" do
    travel_to Time.local(2019, 1, 1, 0, 0, 1)
    category = categories(:bill)
    assert_difference 'Transaction.count', 53 do
      post transactions_path, xhr: true, params: { 
                                            account_id: @account.id, 
                                            category_id: category.id,
                                            transaction: { 
                                              date: Time.now,
                                              name: "Blahdeblah",
                                              details: "Informations",
                                              cost: -1.00
                                            },
                                            recurrence: '2'
                                          }
    end
  end
  
  test "Should create recurring transactions when set at 1 per 2 weeks" do
    travel_to Time.local(2019, 1, 1, 0, 0, 1)
    category = categories(:bill)
    assert_difference 'Transaction.count', 27 do
      post transactions_path, xhr: true, params: { 
                                            account_id: @account.id, 
                                            category_id: category.id,
                                            transaction: { 
                                              date: Time.now,
                                              name: "Blahdeblah",
                                              details: "Informations",
                                              cost: -1.00
                                            },
                                            recurrence: '3'
                                          }
    end
  end
  
  test "Should create recurring transactions when set at 1 per 3 weeks" do
    travel_to Time.local(2019, 1, 1, 0, 0, 1)
    category = categories(:bill)
    assert_difference 'Transaction.count', 18 do
      post transactions_path, xhr: true, params: { 
                                            account_id: @account.id, 
                                            category_id: category.id,
                                            transaction: { 
                                              date: Time.now,
                                              name: "Blahdeblah",
                                              details: "Informations",
                                              cost: -1.00
                                            },
                                            recurrence: '4'
                                          }
    end
  end
  
  test "Should create recurring transactions when set at 1/month" do
    travel_to Time.local(2019, 1, 1, 0, 0, 1)
    category = categories(:bill)
    assert_difference 'Transaction.count', 12 do
      post transactions_path, xhr: true, params: { 
                                            account_id: @account.id, 
                                            category_id: category.id,
                                            transaction: { 
                                              date: Time.now,
                                              name: "Blahdeblah",
                                              details: "Informations",
                                              cost: -1.00
                                            },
                                            recurrence: '5'
                                          }
    end
  end
  
  test "Should create recurring transactions when set at 1 per 2 months" do
    travel_to Time.local(2019, 1, 1, 0, 0, 1)
    category = categories(:bill)
    assert_difference 'Transaction.count', 6 do
      post transactions_path, xhr: true, params: { 
                                            account_id: @account.id, 
                                            category_id: category.id,
                                            transaction: { 
                                              date: Time.now,
                                              name: "Blahdeblah",
                                              details: "Informations",
                                              cost: -1.00
                                            },
                                            recurrence: '6'
                                          }
    end
  end
end
