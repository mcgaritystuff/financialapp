require 'test_helper'

class AccountsSectionTest < ActionDispatch::IntegrationTest
  test "The initial page has no accounts and waits for a user to be clicked" do
    get root_url
    assert_select accounts(:john_checking).name, count: 0
    assert_select accounts(:john_savings).name, count: 0
    assert_select accounts(:jane_checking).name, count: 0
    assert :success
  end

  test "Shows the correct user's accounts" do
    john = users(:john)
    jane = users(:jane)
    john_check = accounts(:john_checking)
    john_saving = accounts(:john_savings)
    jane_check = accounts(:jane_checking)

    get user_path john
    assert_select 'a.selected-user', 'John'
    assert_select "input[value='#{john_check.name} ($#{john_check.current_balance(formatted: true)})']", count: 1
    assert_select "input[value='#{john_saving.name} ($#{john_saving.current_balance(formatted: true)})']", count: 1
    assert_select "input[value='#{jane_check.name} ($#{jane_check.current_balance(formatted: true)})']", count: 0

    get user_path jane
    assert_select 'a.selected-user', 'Jane'
    assert_select "input[value='#{john_check.name} ($#{john_check.current_balance(formatted: true)})']", count: 0
    assert_select "input[value='#{john_saving.name} ($#{john_saving.current_balance(formatted: true)})']", count: 0
    assert_select "input[value='#{jane_check.name} ($#{jane_check.current_balance(formatted: true)})']", count: 1
  end

  test "Should display a new account in the accounts list after it has been created" do
    john = users(:john)
    get user_path john
    before_new_account_count = john.accounts.count
    
    assert_select "section#accounts-list>form", count: before_new_account_count

    get new_account_path(john), xhr: true # Open modal
    post '/accounts' , params: {account: {name: 'blah account', account_type: 'na'}, user_id: john.id },
                       xhr: true # Emulate filling out the form
                      
    # This is an xhr response with js, yet I can't 
    # test the response with assert_match (only matches once so false-positive pass)
    # It appears this is the best way to test it...? :\
    get user_path john
    assert_select "section#accounts-list>form", count: before_new_account_count + 1
  end

  test "Should remove an account when it is deleted" do
    john = users(:john)
    checking = accounts(:john_checking)
    get user_path john
    before_new_account_count = john.accounts.count
    
    assert_select "section#accounts-list>form", count: before_new_account_count

    delete account_path(checking), xhr: true
                      
    get user_path john
    assert_select "section#accounts-list>form", count: before_new_account_count - 1
  end
end
