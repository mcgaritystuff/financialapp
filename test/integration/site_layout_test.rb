require "test_helper"

class SiteLayoutTest < ActionDispatch::IntegrationTest
  test "Show the correct net worth on the page" do
    john = users(:john)
    
    get user_path john

    total_net_worth = john.net_worth
    assert_match "Net Worth: $#{total_net_worth}", response.body, count: 1
  end

  test "Should fill table with transactions after an account is clicked" do
    john = users(:john)
    checking = accounts(:john_checking)
    year = 2019

    get user_path john
    assert_select "form[action='/accounts/#{checking.id}/transactions']", count: 1
    get "#{accounts_path}/#{checking.id}/transactions", xhr: true, params: { year: year }
    
    checking.transactions.select{ |t| t.date.to_date.year == year }.each do |transaction|
      assert_match "<td>#{transaction.date.to_date}</td>", response.body
      assert_match "<td title=\"#{transaction.name}\">#{transaction.name}</td>", response.body
      assert_match "<td title=\"#{transaction.details}\">#{transaction.details}</td>", response.body
      assert_match "<td>$#{transaction.transaction_cost(formatted: true)}</td>", response.body
      assert_match "<td>#{transaction.category.name}</td>", response.body
    end

    assert_match "<td>$#{checking.current_balance(formatted: true)}</td>", response.body
  end

  test "Shows a message when no transactions exist for a given year" do
    year = 2010
    checking = accounts(:john_checking)

    get user_path users(:john)
    get "#{accounts_path}/#{checking.id}/transactions", xhr: true, params: { year: year }
    assert_select "div.empty-transactions"
  end

  test "There is always a year that is ahead of the current year by one" do
    current_year = Time.now.year

    get user_path users(:john)
    assert_select "option[value='#{current_year+1}']"
  end
end
