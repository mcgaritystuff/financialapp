require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  test "Doesn't format a balance when no formatted parameter is passed" do
    account = accounts(:john_checking)
    account_balance = balances(:john_2019_checking).total_balance

    assert_equal account_balance, account.current_balance
  end

  test "Formats a balance when asked to do so" do
    account = accounts(:john_checking)
    account_balance = balances(:john_2019_checking).total_balance

    assert_equal number_with_precision(account_balance, precision: 2, delimiter: ','), account.current_balance(formatted: true)
  end

  test "Gets last year's balance when explicitely asked in current_balance and prev_years_balance" do
    current_year = 2019
    account = accounts(:john_checking)
    account_2018_balance = balances(:john_2018_checking).total_balance
    account_2019_balance = balances(:john_2019_checking).total_balance

    assert_equal account_2019_balance, account.current_balance(year: current_year)
    assert_equal account_2018_balance, account.prev_years_balance(current_year)
  end

  test "Must have a name for the bank account" do
    assert_not Account.new(user_id: 1).valid?
  end
end
