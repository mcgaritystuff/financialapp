require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  test "Category must have a name" do
    assert_not Category.new.valid?
  end
end
