require 'test_helper'

class TransactionTest < ActiveSupport::TestCase
  test "updates the respective balance when a new transaction is created" do
    john_checking = accounts(:john_checking)
    bill = categories(:bill)

    assert_difference 'Balance.where("year = ? AND account_id = ?", Time.now.year, john_checking.id).first.total_balance', -30 do
      Transaction.create(
        date: Time.now,
        name: 'Blah',
        details: 'Some Details',
        cost: -30,
        account: john_checking,
        category: bill
      )
    end
  end

  test "Should create a new balance object when a transaction of a different year is made" do
    john_checking = accounts(:john_checking)
    bill = categories(:bill)

    assert_difference 'Balance.count', 1 do
      Transaction.create(
        date: 2.years.ago,
        name: 'Blah',
        details: 'Some Details',
        cost: -30,
        account: john_checking,
        category: bill
      )
    end

    assert -30, Balance.last.total_balance
  end

  test "That if a brand new balance object is created, its initial balance is the cost of the new transaction" do
    john_checking = accounts(:john_checking)
    bill = categories(:bill)

    Transaction.create(
      date: 2.years.ago,
      name: 'Blah',
      details: 'Some Details',
      cost: -30,
      account: john_checking,
      category: bill
    )

    assert -30, Balance.last.total_balance
  end

  test "That if a brand new rewards balance object is created, its initial rewards balance is the amount of the new transaction" do
    john_credit_card = accounts(:john_credit_card)
    bill = categories(:bill)

    Transaction.create(
      date: 2.years.ago,
      name: 'Blah',
      details: 'Some Details',
      cost: -30,
      reward_amount: 60,
      account: john_credit_card,
      category: bill
    )

    assert 60, Balance.last.total_reward_balance
  end

  test "Transactions must have a name" do
    assert_not Transaction.new(date: Time.now, account_id: 1, category_id: 1).valid?
  end

  test "Transactions must have a date" do
    assert_not Transaction.new(name: "Blah", account_id: 1, category_id: 1).valid?
  end

  test "Transaction does not double a payment/deposit" do
    checking = accounts(:john_checking)
    bill = categories(:bill)

    assert_difference 'Balance.find_by(year: 2019, account_id: checking.id).total_balance', -30 do
      Transaction.create(
        date: '2019-1-1'.to_date,
        name: 'Blah',
        details: 'Some Details',
        cost: -30,
        account: checking,
        category: bill
      )
    end
  end
end
