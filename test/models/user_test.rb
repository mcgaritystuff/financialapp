require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @john = users(:john)
    @net_worth = @john.accounts.sum {|a| a.transactions.sum(&:transaction_cost) }
  end

  test "That the net worth is the correct amount" do
    assert_equal @net_worth, @john.net_worth(false)
  end

  test "The net worth prints formatted by default" do
    assert_equal number_with_precision(@net_worth, precision: 2, delimiter: ','), @john.net_worth
  end

  test "User must have a name" do
    assert_not User.new.valid?
  end
end
